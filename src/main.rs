use rand::distributions::WeightedIndex;
use rand::prelude::*;
use std::{thread, time};

#[derive(Default)]
struct Group {
    size: u32,
    name: String,
    date: String,
}

fn main() {
    let groups = vec![
        // I don't have any good data before 200.

        // In future versions I will try to type out each individuals name instead of grouping it
        // But that will take a while
        // Data taken from https://israelpalestinetimeline.org/charts/
        Group {
            size: 91,
            name: "Child".to_string(),
            date: "2000".to_string(),
        },
        Group {
            size: 103,
            name: "Child".to_string(),
            date: "2001".to_string(),
        },
        Group {
            size: 201,
            name: "Child".to_string(),
            date: "2002".to_string(),
        },
        Group {
            size: 141,
            name: "Child".to_string(),
            date: "2003".to_string(),
        },
        Group {
            size: 188,
            name: "Child".to_string(),
            date: "2004".to_string(),
        },
        Group {
            size: 56,
            name: "Child".to_string(),
            date: "2005".to_string(),
        },
        Group {
            size: 139,
            name: "Child".to_string(),
            date: "2006".to_string(),
        },
        Group {
            size: 68,
            name: "Child".to_string(),
            date: "2007".to_string(),
        },
        Group {
            size: 160,
            name: "Child".to_string(),
            date: "2008".to_string(),
        },
        Group {
            size: 295,
            name: "Child".to_string(),
            date: "2009".to_string(),
        },
        Group {
            size: 15,
            name: "Child".to_string(),
            date: "2010".to_string(),
        },
        Group {
            size: 15,
            name: "Child".to_string(),
            date: "2011".to_string(),
        },
        Group {
            size: 44,
            name: "Child".to_string(),
            date: "2012".to_string(),
        },
        Group {
            size: 3,
            name: "Child".to_string(),
            date: "2013".to_string(),
        },
        Group {
            size: 560,
            name: "Child".to_string(),
            date: "2014".to_string(),
        },
        Group {
            size: 33,
            name: "Child".to_string(),
            date: "2015".to_string(),
        },
        Group {
            size: 36,
            name: "Child".to_string(),
            date: "2016".to_string(),
        },
        Group {
            size: 21,
            name: "Child".to_string(),
            date: "2017".to_string(),
        },
        Group {
            size: 56,
            name: "Child".to_string(),
            date: "2018".to_string(),
        },
        Group {
            size: 31,
            name: "Child".to_string(),
            date: "2019".to_string(),
        },
        Group {
            size: 8,
            name: "Child".to_string(),
            date: "2020".to_string(),
        },
        Group {
            size: 77,
            name: "Child".to_string(),
            date: "2021".to_string(),
        },
        Group {
            size: 8,
            name: "Child".to_string(),
            date: "2022".to_string(),
        },
        // This data was taken from https://www.ochaopt.org/data/casualties
        Group {
            size: 423,
            name: "Civilian".to_string(),
            date: "2008".to_string(),
        },
        Group {
            size: 50,
            name: "Civilian".to_string(),
            date: "2009".to_string(),
        },
        Group {
            size: 43,
            name: "Civilian".to_string(),
            date: "2010".to_string(),
        },
        Group {
            size: 60,
            name: "Civilian".to_string(),
            date: "2011".to_string(),
        },
        Group {
            size: 133,
            name: "Civilian".to_string(),
            date: "2012".to_string(),
        },
        Group {
            size: 31,
            name: "Civilian".to_string(),
            date: "2013".to_string(),
        },
        Group {
            size: 1760,
            name: "Civilian".to_string(),
            date: "2014".to_string(),
        },
        Group {
            size: 173,
            name: "Civilian".to_string(),
            date: "2015".to_string(),
        },
        Group {
            size: 108,
            name: "Civilian".to_string(),
            date: "2016".to_string(),
        },
        Group {
            size: 62,
            name: "Civilian".to_string(),
            date: "2017".to_string(),
        },
        Group {
            size: 242,
            name: "Civilian".to_string(),
            date: "2018".to_string(),
        },
        Group {
            size: 94,
            name: "Civilian".to_string(),
            date: "2019".to_string(),
        },
        Group {
            size: 29,
            name: "Civilian".to_string(),
            date: "2020".to_string(),
        },
        Group {
            size: 217,
            name: "Civilian".to_string(),
            date: "2021".to_string(),
        },
        Group {
            size: 47,
            name: "Civilian".to_string(),
            date: "2022".to_string(),
        },
        // This data was taken from https://
        Group {
            size: 19,
            name: "Journalist".to_string(),
            date: "2001-2018".to_string(),
        },
    ];

    let groups_size = &groups.len();

    println!("A memorial to all of the Palestinians who have been killed by Israel");
    println!("From the river to the sea!");
    println!("\n");
    println!("Death #\t\tGroup\t\t\t\t\tDate of Death");

    let total = count_total(&groups, groups_size);
    let mut i = 0;
    while i < total {
        let selection = select_group(&groups, groups_size);
        if groups[selection].size > 0 {
            display_group(i, &groups, selection);
        } else {
            i -= 1;
        };
        i += 1;
    }
}

// Counts the total number of victims
fn count_total(arr: &[Group], group_size: &usize) -> u32 {
    let mut total: u32 = 0;
    let mut i = 0;
    while &i < group_size {
        total += arr[i].size;
        i += 1;
    }

    total
}

// Selects which group to display.
fn select_group(groups: &[Group], groups_size: &usize) -> usize {
    let mut size: Vec<u32> = vec![];
    let mut i = 0;
    while &i < groups_size {
        size.push(groups[i as usize].size);
        i += 1;
    }
    let dist = WeightedIndex::new(size).unwrap();
    let mut rng = thread_rng();
    let group: usize = dist.sample(&mut rng) as usize;
    group
}

// Prints out all of the details
fn display_group(i: u32, groups: &[Group], selection: usize) {
    print!("{}\t\t", i + 1);
    if i + 1 > 10000000 {
        print!("\t");
    };

    let selection_name = &groups[selection].name;
    print!("{}\t", selection_name);

    if groups[selection].name.len() < 8 {
        print!("\t\t\t\t");
    } else if groups[selection].name.len() < 16 {
        print!("\t\t\t");
    } else if groups[selection].name.len() < 24 {
        print!("\t\t");
    } else if groups[selection].name.len() < 32 {
        print!("\t");
    };

    println!("{}", groups[selection].date);

    let timespan = time::Duration::from_millis(1000);
    thread::sleep(timespan);
}
